FROM php:7.0-fpm-alpine

ENV DB_NAME=wordpress \
	DB_USER=root \
	DB_HOST=mariadb \
	WP_ENV=development

RUN apk --update --no-cache add less bash su-exec mysql-client freetype-dev libjpeg-turbo-dev libpng-dev \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
	&& docker-php-ext-install gd mysqli opcache

# install composer
RUN curl https://getcomposer.org/download/$(curl -LSs https://api.github.com/repos/composer/composer/releases/latest | grep 'tag_name' | sed -e 's/.*: "//;s/".*//')/composer.phar > composer.phar \
    && chmod +x composer.phar \
    && mv composer.phar /usr/local/bin/composer \
    && curl https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > wp-cli.phar \
    && chmod +x wp-cli.phar \
    && mv wp-cli.phar /usr/local/bin/wp-cli.phar


# set folder permissions
RUN mkdir /var/www/bedrock/
RUN chown -R www-data:www-data /var/www/ && chmod g+s /var/www/bedrock/

# install bedrock & set permissions
RUN su-exec www-data composer create-project roots/bedrock /var/www/bedrock/ && chmod -R 755 /var/www/bedrock/
RUN chown -R www-data:www-data /var/www/ && chmod g+s /var/www/bedrock/

EXPOSE 9000

# init script and CMD
COPY init.sh /opt/
RUN chown www-data:www-data /opt/init.sh
RUN chmod +x /opt/init.sh

CMD /opt/init.sh && exec php-fpm
