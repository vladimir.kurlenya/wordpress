locals {
  cluster = "bedrock"
  tags = {
    Environment = "test"
  }

  dns_name = "test.wodpress.int"

  instance_count =2
  https_listeners_count = 1

  https_listeners = "${list(
                        map(
                            "port", 443,
                            "protocol", "HTTPS",
                            "certificate_arn", aws_iam_server_certificate.cert.0.arn,
                            "ssl_policy", "ELBSecurityPolicy-TLS-1-2-2017-01",
                            "target_group_index", 0,
                        ),
  )}"

  http_tcp_listeners_count = 1

  http_tcp_listeners = "${list(
                            map(
                                "port", 80,
                                "protocol", "HTTP",
                                "target_group_index", 0,
                            ),
  )}"

  target_groups_count = 1

  target_groups = "${list(
                        map("name", "http",
                            "backend_protocol", "HTTP",
                            "backend_port", 80,
                        ),
  )}"
}

