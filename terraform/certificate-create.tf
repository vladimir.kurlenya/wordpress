resource "tls_private_key" "cert" {
  algorithm = "ECDSA"
}

resource "tls_self_signed_cert" "cert" {
  key_algorithm   = "${tls_private_key.cert.algorithm}"
  private_key_pem = "${tls_private_key.cert.private_key_pem}"

  validity_period_hours = 120

  early_renewal_hours = 3

  allowed_uses = [
      "key_encipherment",
      "digital_signature",
      "server_auth",
  ]

  dns_names = ["${local.dns_name}"]

  subject {
      common_name  = "${local.dns_name}"
      organization = "${local.cluster} Example"
  }
}